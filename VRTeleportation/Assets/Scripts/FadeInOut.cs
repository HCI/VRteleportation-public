﻿//***********************************************************
// Filename: FadeInOut.cs
// Author: Marco Fendrich, Moritz Kolvnebach
// Last changes: Thursday, 9th of August 2018
// Content: Controls the black fadeIn and fadeOut between teleports
//***********************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the black fadeIn and fadeOut between teleports
/// </summary>
public class FadeInOut : MonoBehaviour {

    /// Material used to render the fade in/fade out quad
    [Tooltip("Material used to render the fade in/fade out quad.")]
    [SerializeField]
    private Material FadeMaterial;
    private Material FadeMaterialInstance;
    private int MaterialFadeID;

    // Mesh used for the fadeOut graphic
    private Mesh PlaneMesh;
    // The transparency factor of the fade graphic
    private float alpha =.0F;

    void Start ()
    {
        // Calculate the plane mesh used for "fade out" graphic on teleport
        PlaneMesh = new Mesh();
        Vector3[] verts = new Vector3[]
        {
            new Vector3(-1, -1, 0),
            new Vector3(-1, 1, 0),
            new Vector3(1, 1, 0),
            new Vector3(1, -1, 0)
        };
        int[] triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        PlaneMesh.vertices = verts;
        PlaneMesh.triangles = triangles;
        PlaneMesh.RecalculateBounds();

        if (FadeMaterial != null)
            FadeMaterialInstance = new Material(FadeMaterial);
        // Set some standard variables
        MaterialFadeID = Shader.PropertyToID("_Fade");
    }

    private void OnPostRender()
    {
        // Position the fadeGraphic in front of the players eyes
        Matrix4x4 local = Matrix4x4.TRS(Vector3.forward * 0.3f, Quaternion.identity, Vector3.one);
        FadeMaterialInstance.SetPass(0);
        FadeMaterialInstance.SetFloat(MaterialFadeID, alpha);
        Graphics.DrawMeshNow(PlaneMesh, transform.localToWorldMatrix * local);
    }

    public void setAlphaValue(float sentAlpha) => alpha = sentAlpha;
}
