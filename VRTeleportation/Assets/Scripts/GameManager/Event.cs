﻿//***********************************************************
// Filename: Event.cs
// Author: Niclas Dobbertin
// Last changes: Mittwoch, 8. August 2018
// Content: This file contains the abstract Event class and all inheriting Events used
//***********************************************************
using System;
using UnityEngine;

/// <summary>
/// This class defines an abstract Event from which all custom Events inherit
/// </summary>
/// <typeparam name="T">The type of Event, has to inherit from this abstract class</typeparam>
public abstract class Event<T> where T : Event<T>
{
    // description that this event type has fired, mostly used to debug
    public string description;
    // true when an unique event has already fired, to prevent loops
    private bool hasFired;
    // signature Event listeners have to match
    public delegate void EventListener(T info);
    // contains all subscribed listeners
    private static event EventListener Listeners;

    /// <summary>
    /// Adds a method with fitting signature as listener to Listeners
    /// </summary>
    /// <param name="listener">The method with fitting signature that should be called when the Event fires</param>
    public static void RegisterListener(EventListener listener)
    {
        Listeners += listener;
    }

    /// <summary>
    /// Removes a method with fitting signature as listener to Listeners
    /// </summary>
    /// <param name="listener">The method that should be removed as listener to that Event</param>
    public static void UnregisterListener(EventListener listener)
    {
        Listeners -= listener;
    }

    /// <summary>
    /// Calls all registered listeners with child-specific info
    /// </summary>
    public void FireEvent()
    {
        if (hasFired)
        {
            throw new Exception("This event has already fired, to prevent infinite loops you can't refire an event");
        }
        hasFired = true;
        if (Listeners != null)
        {
            Listeners(this as T);
        }
    }
}

/// <summary>
/// This event is for debug purposes, unused verbosityLevel indicates importance of event
/// </summary>
public class DebugEvent : Event<DebugEvent>
{
    public int verbosityLevel;
}

/// <summary>
/// This event should fire when a new target gets instantiated
/// </summary>
public class TargetSpawnedEvent : Event<TargetSpawnedEvent>
{
    // The id (number of targets thus far starting with zero) of this target
    public int targetId;
    // The transform object of this target
    public Transform targetTransform;
}

/// <summary>
/// This event should fire when a target is locked in and thus reached/destroyed
/// </summary>
public class TargetReachedEvent : Event<TargetReachedEvent>
{
    // The id (number of targets thus far starting with zero) of this target
    public int targetId;
    // The transform object of this target
    public Transform targetTransform;
    // The transform object of the player 
    // TODO: check if transform should be replaced with Vector3 to avoid distortion through player movement
    public Transform playerTransform;
    // Realtime from target instantiation to lock in
    public float time;
}
 
/// <summary>
/// This event should fire when the current experiment has finished
/// </summary>
public class ExperimentEndEvent : Event<ExperimentEndEvent>
{
    // empty for now
}

/// <summary>
/// This event should fire after a teleportation was executed
/// </summary>
public class TeleportEvent : Event<TeleportEvent>
{
    // Realtime on initiating the teleport by holding a controller button
    public float startTeleportTime;
    // Realtime when the teleport is finished and the user is in the new position
    public float endTeleportTime;
    // Coordinates of the player when initiating the teleport
    public Vector3 startUserPosition;
    // Rotation of the player when initiating the teleport
    public Vector3 startUserRotation;
    // Coordinates of the player when the teleport is finished
    public Vector3 endUserPosition;
    // Rotation of the player when the teleport is finished
    public Vector3 endUserRotation;
}