﻿//***********************************************************
// Filename: GameManager.cs
// Author: Niclas
// Last changes: 08 August 2018
// Content: Contains variables and functions relevant globally 
//***********************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class holds varibales and functions which have to be universally accessible
/// </summary>
public class GameManager : MonoBehaviour {

    // Name of the currently used method of movement
    public string movementMethod;
    // The id used for the current test subject
    public string testSubjectId;
    // The Object in the scene which represents the player
    public GameObject playerGameObject;
}
