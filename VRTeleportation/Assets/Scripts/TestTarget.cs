﻿//***********************************************************
// Filename: TestTarget.cs
// Author: Niclas
// Last changes: Mittwoch, 8. August 2018
// Content: Behaviour of the targets
//***********************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Behaviour script of the generated targets to be reached. Used to send target data.
/// </summary>
public class TestTarget : MonoBehaviour {

    // ID of this target, doubles as the count of targets starting with zero. Gets set by TargetManager on spawn
    public int id=0;
    // Realtime when the target was instantiated
    public float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
	}

    /// <summary>
    /// On target destruction, which signals reaching the target, fire a TargetReachedEvent with target data
    /// </summary>
    private void OnDestroy()
    {
        TargetReachedEvent tre = new TargetReachedEvent
        {
            description = "Target has been locked",
            targetId = id,
            targetTransform = transform,
            playerTransform = GameObject.FindGameObjectWithTag("MainCamera").transform,
            time = Time.time - startTime
        };

        tre.FireEvent();
    }
}
