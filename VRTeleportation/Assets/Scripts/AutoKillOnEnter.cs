﻿//***********************************************************
// Filename: AutoKillOnEnter.cs
// Author: Marco Fendrich, Moritz Kolvenbach
// Last changes: Thursday, 9th of August 2018
// Content: Destroys the start area and ends the tutorial as soon as the player leaves the start area
//***********************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroys start area and ends the tutorial as soon as the player leaves the start area
/// </summary>
public class AutoKillOnEnter : MonoBehaviour
{
    // The object that is destroyed as soon as a collision with the object that this script is attached to occurs
    public GameObject objectToBeDestroyed;
    // The targetManager; needed so this script can update isExpActive on collision
    public TargetManager targetManager;

    /// <summary>
    /// Updates isExpActive, destroys the given object and the object this script is attached to
    /// </summary>
    /// <param name="other"> The collider of the obejct this script is attached to</param>
    void OnTriggerStay(Collider other)
    {
        targetManager.isExpActive = true;
        Destroy(objectToBeDestroyed);
        Destroy(gameObject);
    }
}
