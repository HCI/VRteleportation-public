﻿//***********************************************************
// Filename: AudioFeedback.cs
// Author: Marco Fendrich, Moritz Kolvenbach
// Last changes: Thursday, 9th of August 2018
// Content: Handles the audio feedback during the trials
//***********************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class managing the audio cues throughout the experiment
/// </summary>
public class AudioFeedback : MonoBehaviour
{
    // the sound being played on targetConfirmation
    public AudioSource targetAudio;
    // the sound being played when all targets of the current condition have been completed
    public AudioSource gameEndAudio;

    /// <summary>
    /// Registers the listeners for each event that triggers an audio cue
    /// </summary>
    void Start ()
    {
        TargetReachedEvent.RegisterListener(OnTargetConfirmation);
        ExperimentEndEvent.RegisterListener(OnExperimentEnd);
    }

    /// <summary>
    /// On target confirmation play sound
    /// </summary>
    /// <param name="trEv"> An event being triggerd whenever the player confirms a target</param>
    void OnTargetConfirmation(TargetReachedEvent trEv)
    {
        targetAudio.Play(0);
    }

    /// <summary>
    /// On experiment end play sound
    /// </summary>
    /// <param name="eeEv"> An event being triggerd when experiment is over</param>
    void OnExperimentEnd(ExperimentEndEvent eeEv)
    {
        gameEndAudio.Play(0);
    }
}
