﻿//***********************************************************
// Filename: RenderableTeleport.cs
// Author: Moritz Kolvenbach, Marco Fendrich
// Last changes: Wednesday, 8th of August 2018
// Content: General base class for teleport displaying a parabolic or similar line to indicate where the player is aiming at
//***********************************************************

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract base class for teleports already including interface to be used drag and drop style with vive controllers
/// </summary>
public abstract class RenderableTeleport : MonoBehaviour, IButton
{
    // general reference classes for access
    protected GameObject gameManager;
    protected Visualiser renderScript;
    protected Teleport teleportScript;
    protected ViveNavMesh navMesh;
    protected GameManager gameManagerScript;

    // list of points within parabola or line being shown to player as indicator for his teleportation
    protected List<Vector3> projectionPoints;
    // distance in meters between the single points of the optical representation
    protected float pointSpacing;
    // maximum number of points being calculated until parabola is being stopped
    protected int pointCount;

    // checks whether the player is trying to aim a teleport function and the parabola has to be calculated
    protected bool isActive;
    // indicator of whether last point of current projectionPoints is on navMesh
    protected bool pointOnNavMesh;

    // forward vector of controller object
    protected Vector3 normalisedVelocity = Vector3.forward;
    // upward vector of hit point
    protected Vector3 normalisedHitPoint = Vector3.up;

    // rotation being passed through from specific child teleportation to teleportation class
    protected float rotation = float.NegativeInfinity;

    // timestamp for tracked event 
    protected float timeInitialized;

    void Start()
    {
        // get references for variables declared above
        gameManager = GameObject.FindWithTag("GameController");
        gameManagerScript = (GameManager)gameManager.GetComponent(typeof(GameManager));
        teleportScript = (Teleport)gameManager.GetComponent(typeof(Teleport));
        renderScript = (Visualiser)gameManager.GetComponent(typeof(Visualiser));
        navMesh = (ViveNavMesh)gameManager.GetComponent(typeof(ViveNavMesh));
        projectionPoints = new List<Vector3>(pointCount);
        pointSpacing = renderScript.PointSpacing;
        pointCount = renderScript.PointCount;
    }

    void Update()
    {
        if (!isActive)
            return;

        UpdateProjectionPoints();
        renderScript.updateRendering(pointOnNavMesh, projectionPoints, normalisedHitPoint, normalisedVelocity, rotation);
    }

    /// <summary>
    /// Function being called on press by player; sets rendering to active and starts calculating parabola
    /// </summary>
    /// <param name="controllerObject">gameObject of controller</param>
    /// <param name="controllerIdentificator">ID of controller currently using this function</param>
    public void OnButtonDown(GameObject controllerObject, int controllerIdentificator)
    {
        gameObject.transform.parent = controllerObject.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localRotation = Quaternion.identity;
        renderScript.enabled = true;
        isActive = true;
        timeInitialized = Time.time;
    }

    /// <summary>
    /// Function being called on release of button by player; starts teleportation if valid target is selected
    /// </summary>
    public void OnButtonUp()
    {
        // only teleport if valid target is selected
        if (pointOnNavMesh)
        {
            // call to teleport function
            teleportScript.CallTeleport(projectionPoints[projectionPoints.Count - 1], rotation, timeInitialized);
            // set rotation back to none
            rotation = float.NegativeInfinity;
        }
        isActive = false; // deactivate calculations
        renderScript.enabled = false; // disable render script
    }

    /// <summary>
    /// Child classes have to calculate the new points of the parabola as those are specific to the given teleportation
    /// </summary>
    protected abstract void UpdateProjectionPoints();
}

