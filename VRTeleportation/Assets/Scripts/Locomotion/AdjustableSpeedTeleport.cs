﻿//***********************************************************
// Filename: AdjustableSpeedTeleport.cs
// Author: Moritz Kolvenbach, Marco Fendrich
// Last changes: Wednesday, 8th of August 2018
// Content: Class that adds an adjustable speed setting via y axis of touchpad to ParabolicTeleport
//***********************************************************

using UnityEngine;

/// <summary>
/// Child class of ParabolicTeleport adding an adjustable speed setting via y axis of touchpad
/// </summary>
public class AdjustableSpeedTeleport : ParabolicTeleport, ITouchpadAxis {
    
    // container for public velocity adjusted in editor which will be overwritten for calculation
    private float baseVelocity;

    void Awake()
    {
        // save base velocity as it will be overwritten with adjusted touchpad velocity
        baseVelocity = initialVelocity.z;
    }

    /// <summary>
    /// adjust velocity according to position on touchpad so that:
    /// y=-1 => 0% of maximum velocity
    /// y=+1 => 100% of maximum velocity
    /// </summary>
    /// <param name="axis">touchpad axis</param>
    public void OnButtonPressed(Vector2 axis)
    {
        initialVelocity.z = (axis.y + 1.0F) * 0.5F * baseVelocity;
    }
}
