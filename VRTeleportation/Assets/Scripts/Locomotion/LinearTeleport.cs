﻿//***********************************************************
// Filename: LinearTeleport.cs
// Author: Moritz Kolvenbach, Marco Fendrich
// Last changes: Wednesday, 8th of August 2018
// Content: Class that calculates the line to be rendered and used to aim when preparing to teleport
//***********************************************************

using UnityEngine;

/// <summary>
/// Child class of renderableTeleport doing the actual calculations to aim the teleportation as a linecast
/// </summary>
public class LinearTeleport : RenderableTeleport
{
    /// <summary>
    /// Sample a bunch of points along a line until you hit ground. At that point, cut off the linecast
    /// </summary>
    protected override void UpdateProjectionPoints()
    {
        // turn velocity to work into the direction of the controller
        normalisedVelocity = transform.TransformDirection(Vector3.forward);
        // delete points from last calculation
        projectionPoints.Clear();
        // add start point - the controller
        projectionPoints.Add(transform.position);

        // set point from which second point will be calculated to controller position
        Vector3 last = transform.position;
        float t = 0; // time for line calculation hasn't started yet

        // iterate through the maximum number of points being calculated
        for (int i = 0; i < pointCount; i++)
        {
            // increase t so that the next calculated point will have the distance set in editor
            t += pointSpacing / normalisedVelocity.magnitude;

            // calculate next point of line
            Vector3 next = CalculateNewPosition(transform.position, normalisedVelocity, t);
            Vector3 castHit; // hit point of line if colliding at or before next calculated point
            Vector3 norm; // normal of hit point
            bool endOnNavmesh; // indicator of whether last point calculated ended on navmesh
            // check whether something is being hit
            if (navMesh.Linecast(last, next, out endOnNavmesh, out castHit, out norm))
            {
                // if something is being hit, set last point in list to hit point
                projectionPoints.Add(castHit);
                // normal of hit point to be used when visualizing the currently selected target
                normalisedHitPoint = norm;
                // check whether point is on nav mesh and therefore teleportable
                pointOnNavMesh = endOnNavmesh;
                return;
            }
            else
            {
                // no collision, calculate next point
                projectionPoints.Add(next);
            }

            // set currently calculated point as base for the calculation of the new point
            last = next;
        }
        // if nothing is being hit and maximum number of points is reached, show unreachable target in midair with normal going upwards
        normalisedHitPoint = Vector3.up;
        pointOnNavMesh = false;
    }
    
    /// <summary>
    /// Linear motion equation applied to three dimensions
    /// </summary>
    /// <param name="p">Position from which the movement of the current update was started</param>
    /// <param name="v">Velocity with which the line is being calculated</param>
    /// <param name="t">Abstract time to be used to update point</param>
    /// <returns>Next point of line</returns>
    private static Vector3 CalculateNewPosition(Vector3 p, Vector3 v, float t)
    {
        Vector3 ret = new Vector3();
        for (int i = 0; i < 3; i++)
            ret[i] = p[i] + v[i] * t;
        return ret;
    }
}
