﻿//***********************************************************
// Filename: AngleSelectTeleport.cs
// Author: Moritz Kolvenbach, Marco Fendrich
// Last changes: Wednesday, 8th of August 2018
// Content: Class that adds a rotation via touchpad coordinates to ParabolicTeleport
//***********************************************************

using UnityEngine;

/// <summary>
/// Child class of ParabolicTeleport adding a rotation via touchpad
/// </summary>
public class AngleSelectTeleport : ParabolicTeleport, ITouchpadAxis
{
    /// <summary>
    /// take x and y coordinates of touchpad and translate them to a rotation around the normal of the touchpad
    /// this rotation is being sent through RenderableTeleport to actual teleport script
    /// </summary>
    /// <param name="axis">touchpad axis</param>
    public void OnButtonPressed(Vector2 axis)
    { rotation = Vector2.SignedAngle(axis, Vector2.up); }
}
