﻿//***********************************************************
// Filename: ControllerInput.cs
// Author: Marco Fendrich, Moritz Kolvenbach
// Last changes: Thursday, 9th of August 2018
// Content: Central controllerInput management
//***********************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  A class managing the central controller input
/// </summary>
public class ControllerInput : MonoBehaviour
{
    // scripts to be set for given button
    public MonoBehaviour triggerButtonCall;
    public MonoBehaviour touchpadButtonCall;
    public MonoBehaviour gripButtonCall;
    public MonoBehaviour menuButtonCall;

    // private entities for the scripts to be casted into
    private IButton triggerButton;
    private IButton touchpadButton;
    private ITouchpadAxis touchpadAxis;
    private IButton gripButton;
    private IButton menuButton;

    // trackedObj reference used to get full controller and its input
    private SteamVR_Controller.Device Controller
    { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    // object this script is attached to as a general tracked object
    private SteamVR_TrackedObject trackedObj;

    void Start()
    {
        // cast the single calls to see if they implement the necessary interface
        triggerButton = triggerButtonCall as IButton;
        touchpadButton = touchpadButtonCall as IButton;
        touchpadAxis = touchpadButtonCall as ITouchpadAxis;
        gripButton = gripButtonCall as IButton;
        menuButton = menuButtonCall as IButton;
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    void Update()
    {
        /* call all casted buttons if they're not null with the methods implemented by the interfaces */
        // Trigger calls
        if (triggerButton != null)
        {
            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
                triggerButton.OnButtonDown(gameObject, (int)trackedObj.index);
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
                triggerButton.OnButtonUp();
        }

        // Touchpad calls
        if (touchpadButton != null)
        {
            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
                touchpadButton.OnButtonDown(gameObject, (int)trackedObj.index);
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
                touchpadButton.OnButtonUp();
        } // Additional call if added script supports axis coordinate input
        if (touchpadAxis != null && Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
            touchpadAxis.OnButtonPressed(Controller.GetAxis());

        // Grip calls
        if (gripButton != null)
        {
            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
                gripButton.OnButtonDown(gameObject, (int)trackedObj.index);
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
                gripButton.OnButtonUp();
        }

        // Menu calls
        if (menuButton != null)
        {
            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
                menuButton.OnButtonDown(gameObject, (int)trackedObj.index);
            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
                menuButton.OnButtonUp();
        }
    }
}

/// <summary>
/// An interface used by scripts which need acces to controllerButtonInputs; for press and release of a button
/// </summary>
public interface IButton
{
    void OnButtonDown(GameObject controllerObject, int controllerIdentificator);
    void OnButtonUp();
}

/// <summary>
/// An interface used by scripts that need acces to the location input from the trackpad 
/// </summary>
public interface ITouchpadAxis
{
    void OnButtonPressed(Vector2 axis);
}
