﻿//***********************************************************
// Filename: TutorialTexts.cs
// Author: Moritz Kolvenbach, Marco Fendrich
// Last changes: Thursday, 9th of August 2018
// Content: Selector for tutorial text according to teleportation
//***********************************************************

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Selector for tutorial text according to teleportation
/// </summary>
public class TutorialTexts : MonoBehaviour
{
    // public text of teleport
    public static Text tutorialText;

    // holder of used teleport
    public ControllerInput functionsInUse;

    // teleports being differentiated
    private LinearTeleport linearTeleport;
    private ParabolicTeleport parabolicTeleport;
    private AngleSelectTeleport angleSelectTeleport;

    void Start()
    {
        // try to cast controller function as different teleports to see which is in use
        linearTeleport = functionsInUse.touchpadButtonCall as LinearTeleport;
        parabolicTeleport = functionsInUse.touchpadButtonCall as ParabolicTeleport;
        angleSelectTeleport = functionsInUse.touchpadButtonCall as AngleSelectTeleport;

        // set texts according to teleport being set in controller
        // this has to be done in order of children to parent as otherwise the cascade will trigger early
        if (angleSelectTeleport != null)
        {
            tutorialText.text = "Press down on the touchpad again. When teleporting, you will face into the direction of the red indicator line on the orange selector. This depends on where your finger is on the touchpad when you release it.";
            GetComponentInParent<GameManager>().movementMethod = "AngleSelectTeleport";
        }
        else if (parabolicTeleport != null)
        {
            tutorialText.text = "Press down on the touchpad to cast a parabolic line. Release the touchpad to teleport to the location indicated by the orange selector. A red cross again means you cannot teleport into that area.";
            GetComponentInParent<GameManager>().movementMethod = "ParabelTeleport";
        }
        else if (linearTeleport != null)
        {
            tutorialText.text = "Press down on the touchpad and aim where you want to teleport. An orange indicator means you will teleport on release, a red cross means you cannot teleport to that point.";
            GetComponentInParent<GameManager>().movementMethod = "LinearTeleport";
        }
    }
}
